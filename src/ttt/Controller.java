package ttt;

import ttt.gamer.Human;

import javax.sound.midi.Soundbank;

public class Controller {

    public enum GameResult{
        Xs_VICTORY, Os_VICTORY, DRAW
    }

    private static final int FIELD_DIM = 1;

    private Game game;

    private String gamer1Str;
    private String gamer2Str;

    private Gamer gamer1;
    private Gamer gamer2;
    private Field field;

    private View view;

    public Controller(String gamer1, String gamer2){
        gamer1Str = gamer1;
        gamer2Str = gamer2;
    }

    void startGame(){
        field = new Field(FIELD_DIM);

        view = new View();

        //тут вообще геймеры создаются от gamerStr фабрикой
        gamer1 = new Human();
        gamer2 = new Human();

        gamer1.init(field, view);
        gamer2.init(field, view);

        game = new Game(gamer1, gamer2, field);
    }

    void stopGame(){

    }

    void playGame(){
        startGame();
        GameResult result = game.play();

        if (result == GameResult.Xs_VICTORY) {
            System.out.println("1");
        } else if (result == GameResult.Os_VICTORY){
            System.out.println("2");
        } else{
            System.out.println("-");
        }
        //stopGame();
    }


}
