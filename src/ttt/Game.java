package ttt;

public class Game {


    private Gamer gamer1;
    private Gamer gamer2;
    private Gamer currentGamer;
    private Gamer winner;
    private boolean isOver;
    private boolean isFullOver;
    private Field field;

    public Game(Gamer g1, Gamer g2, Field f){
        gamer1 = g1;
        gamer2 = g2;
        field = f;
        isOver = false;
        isFullOver = false;
    }


    public Controller.GameResult play(){
        do {
            currentGamer = gamer1;
            turn(currentGamer);
            if (!isOver){
                currentGamer = gamer2;
                turn(currentGamer);
            }

        } while (!isOver);


        if (isFullOver){
            return Controller.GameResult.DRAW;
        }

        if (winner == gamer1){
            return Controller.GameResult.Xs_VICTORY;
        } else {
            return Controller.GameResult.Os_VICTORY;
        }
    }

    private void turn(Gamer gamer){
        Field target;

        do {
            target = gamer.aim();
            //если клетка уже помечена, тут должны об этом объявить карренту
        } while (target.isStatused());

        if (gamer == gamer1){
            target.becomeX();
        } else{
            target.becomeO();
        }

        if (field.getStat() != Field.FieldStat.N){
            winner = gamer;
            isOver = true;
        }

        if (field.countOfCleanCells(field) == 0){
            isOver = true;
            isFullOver = true;
        }

    }

}
