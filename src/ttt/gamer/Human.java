package ttt.gamer;

import ttt.Field;
import ttt.Gamer;
import ttt.View;

import java.util.Random;

public class Human implements Gamer{

    private View view;
    private Field field;

    public void init(Field f, View v){
        field = f;
        view = v;
    }

    public Field aim(){
        Random rnd = new Random();

        Field aimedField = field;
        do{
            int index = rnd.nextInt(9);
            aimedField = aimedField.get1of9(index);

        } while (!aimedField.isTTTCell());

        return aimedField; //field.isTTTCell == true
    }

}
