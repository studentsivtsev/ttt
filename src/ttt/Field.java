package ttt;

public class Field {

    private int fieldDim;
    public int getFieldDim(){
        return fieldDim;
    }

    public boolean isTTTCell() {
        return fieldDim == 0;
    }

    private boolean statused;
    public boolean isStatused(){
        return statused;
    }

    public enum FieldStat{
        N, O, X
    }

    private Field[][] fields = new Field[3][3];

    public Field get1of9(int k){
        return fields[k/3][k%3];
    }

    private FieldStat fieldStat;

    public int countOfCleanCells(Field field){
        int sum = 0;
        if (!field.isTTTCell()) {
            for (int i = 0; i < 9; ++i){
                sum += countOfCleanCells(field.get1of9(i));
            }
        } else {
            if (!field.isStatused()){
                ++sum;
            }
        }
        return sum;
    }

    public FieldStat getStat(){
        if (isStatused()){
            return fieldStat;
        }

        if (!isTTTCell()){
            fieldStat = checkField();
        }
        return fieldStat;
    }

    private FieldStat checkField() {
        if (!isTTTCell()){
            FieldStat hor1 = getRowStat(fields[0][0], fields[0][1], fields[0][2]);
            FieldStat hor2 = getRowStat(fields[1][0], fields[1][1], fields[1][2]);
            FieldStat hor3 = getRowStat(fields[2][0], fields[2][1], fields[2][2]);
            FieldStat ver1 = getRowStat(fields[0][0], fields[1][0], fields[2][0]);
            FieldStat ver2 = getRowStat(fields[0][1], fields[1][1], fields[2][1]);
            FieldStat ver3 = getRowStat(fields[0][2], fields[1][2], fields[2][2]);
            FieldStat dia1 = getRowStat(fields[0][0], fields[1][1], fields[2][2]);
            FieldStat dia2 = getRowStat(fields[2][0], fields[1][1], fields[0][2]);

            if (hor1 != FieldStat.N){
                statused = true;
                return hor1;
            }
            if (hor2 != FieldStat.N){
                statused = true;
                return hor2;
            }
            if (hor3 != FieldStat.N){
                statused = true;
                return hor3;
            }
            if (ver1 != FieldStat.N){
                statused = true;
                return ver1;
            }
            if (ver2 != FieldStat.N){
                statused = true;
                return ver2;
            }
            if (ver3 != FieldStat.N){
                statused = true;
                return ver3;
            }
            if (dia1 != FieldStat.N)
                statused = true;
            if (dia2 != FieldStat.N){
                statused = true;
                return dia2;
            }

            return FieldStat.N;
        } else{
            return fieldStat;
        }
    }

    public FieldStat getRowStat(Field f1, Field f2, Field f3){
        if (f1.getStat() == FieldStat.O && f2.getStat() == FieldStat.O && f3.getStat() == FieldStat.O){
            return FieldStat.O;
        }
        if (f1.getStat() == FieldStat.X && f2.getStat() == FieldStat.X && f3.getStat() == FieldStat.X){
            return FieldStat.X;
        }
        return FieldStat.N;
    }

    public void becomeX(){
        fieldStat = FieldStat.X;
        statused = true;
    }

    public void becomeO(){
        fieldStat = FieldStat.O;
        statused = true;
    }



    public Field(int fD){

        fieldDim = fD;
        fieldStat = FieldStat.N;

        if (fD-- > 0) {
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j){
                    fields[i][j] = new Field(fD);
                }
            }
        }


    }



}
